import java.util.*; //import package java util untuk menggunakan ArrayList

public class Task32 { //Class untuk mempung isi dari program yang akan dijalankan, namanya sesuai dengan nama file dan diawali dengan huruf besar
    public static void main(String args[]) { //  merupakan fungsi utama, semua kode yang berada di dalam fungsi main akan dieksekusi/dijalankan
        ArrayList<String> list = new ArrayList<String>(); //membuat ArrayList dengan variabel list dengan tipe data String
        list.add("Mango"); //menambahkan data pada list dengan string Mango
        list.add("Apple"); //menambahkan data pada list dengan string Apple
        list.add("Banana"); //menambahkan data pada list dengan string Banana
        list.add("Grapes"); //menambahkan data pada list dengan string Grapes
        System.out.println(list); //menampilkan isi dari list yang berupa ArrayList
    } //penutup fungsi main

}//penutup class Task2

/*
 * Output yang dihasilkan program ini adalah Array yang berisi kumpulan String :
 * [Mango, Apple, Banana, Grapes]
 */
