/* Program ini membuat suatu variabel map dengan Hashmap 
 * Kemudian memasukkan data kedalam map dengan method put
 * dimana method put ini memerlukan value dan key
 * key yang diberikan pada Hashmap tersebut adalah integer
 * sehingga ketika menambahkan data sampai key 4, program harusnya berjalan dengan baik
 * namun nilai key selanjutnya yang diberikan berupa string yaitu "pisang" sehingga membuat program ini  TIDAK DAPAT BERJALAN
 * 
 * pada baris program selanjutnya digunakan untuk menampilkan hasil dari data yang sudah diinputkan kedalam map
 * dan menampilkan key dan value dari map
 */

import java.util.*;

public class Task5 {
    public static void main(String args[]) {
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "Mango");
        map.put(2, "Apple");
        map.put(3, "Banana");
        map.put(4, "Grapes");
        map.put("pisang", "Pisang");

        System.out.println("Iterating Hashmap...");
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }

}
