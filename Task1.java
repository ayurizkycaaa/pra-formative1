public class Task1 {  //Class untuk mempung isi dari program yang akan dijalankan, namanya sesuai dengan nama file dan diawali dengan huruf besar
    public static void main(String[] args) { //  merupakan fungsi utama, semua kode yang berada di dalam fungsi main akan dieksekusi/dijalankan
        int i = 1; //deklarasi variabel i dengan type data interger dan valuenya = 1
        do { //perulangan dengan do-while berarti akan menjalankan program minimal 1 kali sebelum mengecek apakah kondisi terpenuhi di dalam while 
            if (i == 5) { //kondisi untuk mengecek apakah nilai i=5, jika benar maka akan melakukan perintah dibawahnya 
                i++; //yaitu menambah nilai i dengan 1. i++ berarti i = i +1
                break; //berhenti melakukan eksekusi program
            } //kurung penutup if
            System.out.println(i); //jika kondisi i=5 tidak terpenuhi, maka menjalankan program untuk mencetak nilai i
            i++; //kemudian menambahkan nilai i sebelumnya dengan 1.
        } while (i <= 10); //while yang mengecek apakah nilai nilai i lebih kecil atau sama dengan 10, jika terpenuhi, maka akan menjalankan lagi perulangan dari atas dengan nilai i yang sudah bertambah
    } //penutup fungsi main
} //penutup class Task1

/*hasil dari prongram setelah dijalankan adalah sebagai berikut:
 * 1
 * 2
 * 3
 * 4
 * karena setelah nilai i adalah 5, maka program langsung berhenti
*/