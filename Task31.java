import java.util.*; //import package java util untuk menggunakan ArrayList dan Iterator

public class Task31 { //Class untuk mempung isi dari program yang akan dijalankan, namanya sesuai dengan nama file dan diawali dengan huruf besar
    public static void main(String args[]) { //  merupakan fungsi utama, semua kode yang berada di dalam fungsi main akan dieksekusi/dijalankan
        ArrayList<String> list = new ArrayList<String>(); //membuat variabel list dengan tipe ArrayList yang datanya berupa string
        list.add("Mango"); //menambahkan data pada list dengan string Mango
        list.add("Apple"); //menambahkan data pada list dengan string Apple
        list.add("Banana"); //menambahkan data pada list dengan string Banana
        list.add("Grapes"); //menambahkan data pada list denagn string Grapes
        Iterator itr = list.iterator(); //menggunakan iterator untuk melakukan perulangan isi list, mengambil data dari collection, membuat variabel itr
        while (itr.hasNext()) { //kondisi untuk mengecek isi dari variabel itr, akan mengembalikan nilai True jika its memiliki value, jika tidak maka false
            System.out.println(itr.next()); //jika benar maka akan mencetak isi dari itr. next() = mengembalikan data dan pindah diposisi berikutnya
        }//penutup kondisi
    } //penutup fungsi main

}//penutup class Task31

/*
 * Adapun output dari program berupa string sebagai berikut:
 * Mango
 * Apple
 * Banana
 * Grapes
 * 
 * PERBEDAAN TASK31 DAN TASK32 ADALAH
 * Task 32 menampilkan list yang merupakan Array dengan isi string
 * Task 31 menampilkan isi dari Array dengan perulangan dan memploting isi dari Array tersebut satu persatu
 */
