public class Task2 { //Class untuk mempung isi dari program yang akan dijalankan, namanya sesuai dengan nama file dan diawali dengan huruf besar
    public static void main(String[] args) { //  merupakan fungsi utama, semua kode yang berada di dalam fungsi main akan dieksekusi/dijalankan
        int age = 25; //deklarasi variabel age dengan tipe data integer dan bernilai 25
        int weight = 48; //deklarasi variabel weight dengan tipe data integer dan bernilai 48
        if (age >= 18) { //kondisi untuk mengecek apakah age lebih besar atau sama dengan 18, jika benar maka akan menjalankan baris dibawahnya
            if (weight > 50) { //yaitu kondisi untuk mengecek apakah weight lebih dari 50
                System.out.println("You are eligible to donate blood"); //jika kondisi berat badan atau weight terpenuhi, maka akan menampilkan string "You are eligible to donate blood"
            } else { //jika kondisi weight tidak terpenuhi, maka
                System.out.println("You are not eligible to donate blood"); //akan menampilkan string "You are not eligible to donate blood"
            } //penutup else kondisi weight
        } else { //jika kondisi age tidak terpenuhi
            System.out.println("Age must be greater than 18"); //maka akan menampilkan string "Age must be greater than 18"
        } //penutup else kondisi age
    } //penutup fungsi main

} //penutup class Task2

/*
 * Hasil dari Program adalah sebagai berikut :
 * You are not eligible to donate blood
 * 
 * karena weight tidak terpenuhi, weight harus diatas 50
 */