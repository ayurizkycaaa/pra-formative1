import java.util.Scanner;

public class Task4 {
    public static void main(String[] args){
        int angka1, angka2, hasilPenjumlahan;
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.print("Masukkan Angka Pertama : ");
            angka1 = keyboard.nextInt();
            System.out.print("Masukkan Angka Kedua : ");
            angka2 = keyboard.nextInt();
        }

        hasilPenjumlahan = angka1 + angka2;
        System.out.println("Hasil Penjumlahan Dari " + angka1 + " dan " + angka2 + " adalah : " + hasilPenjumlahan);
    }
}
